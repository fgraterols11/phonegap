import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'personas',
    component: TabsPage,
    children: [
      {
        path: 'personas',
        children: [
          {
            path: '',
            loadChildren: '../personas/personas.module#PersonasModule'
          }
        ]
      },
      {
        path: 'regiones',
        children: [
          {
            path: '',
            loadChildren: '../regiones/regiones.module#RegionesPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/personas',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/personas',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
